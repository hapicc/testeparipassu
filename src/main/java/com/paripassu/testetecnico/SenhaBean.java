package com.paripassu.testetecnico;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.LinkedList;
import java.util.Queue;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Hanna
 */
@ManagedBean
@SessionScoped
public class SenhaBean {

    String senha = "";
    String chamada = "";
    String anterior = "";
    Queue filaNormal = new LinkedList();
    Queue filaPreferencial = new LinkedList();
    int contNormal = 1;
    int contPreferencial = 1;
    int restante = 0;

    public void adicionarFila() {
        if (senha.contains("N")) {
            filaNormal.add(senha);
        } else if (senha.contains("P")) {
            filaPreferencial.add(senha);
        }
        restante++;
    }

    public void chamaSenha() {

        if (!filaPreferencial.isEmpty()) {
            anterior = chamada;
            chamada = filaPreferencial.poll().toString();
            restante--;
        } else {
            if (!filaNormal.isEmpty()) {
                anterior = chamada;
                chamada = filaNormal.poll().toString();
                restante--;
            }
        }

    }

    public void senhaNormal() {
        String tipo = "N";
        String digito = String.format("%04d", contNormal);
        this.senha = tipo + digito;
        adicionarFila();
        contNormal++;
    }

    public void senhaPreferencial() {
        String tipo = "P";
        String digito = String.format("%04d", contPreferencial);
        this.senha = tipo + digito;
        adicionarFila();
        contPreferencial++;
    }

    public void reiniciaContagem() {
        contPreferencial = 1;
        contNormal = 1;
        filaPreferencial.clear();
        filaNormal.clear();
        restante = 0;
        chamada = "";
        anterior = "";
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getChamada() {
        return chamada;
    }

    public void setChamada(String chamada) {
        this.chamada = chamada;
    }

    public int getRestante() {
        return restante;
    }

    public void setRestante(int restante) {
        this.restante = restante;
    }

    public String getAnterior() {
        return anterior;
    }

    public void setAnterior(String anterior) {
        this.anterior = anterior;
    }

}
